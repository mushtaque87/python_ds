#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 15 15:15:15 2017

@author: philips
"""


import spacy
from NLP.spacy_NameEntity import import nltk

from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer

#==============================================================================
# nlp = spacy.load('en')
# doc = nlp(u'London is a big city in the United Kingdom.')
# for ent in doc.ents:
#     print(ent.label_, ent.text)
#==============================================================================
    
train_text = state_union.raw("2005-GWBush.txt")
sample_text = state_union.raw("2006-GWBush.txt")

#==============================================================================
nlp = spacy.load('en')
doc = nlp(u'London , New Youk')
            
for ent in doc.ents:
                print(ent.label_, ent.text)
 
#==============================================================================
#train_text = "Hello !! Everyone how are you all . I am Mushtaque Ahmed"
#sample_text = "and i am going to be your machine learning trainer"


 
custom_sent_tokenizer = PunktSentenceTokenizer(train_text)
#print custom_sent_tokenizer
tokenized = custom_sent_tokenizer.tokenize(sample_text)
#print tokenized

                
def process_context():
    
     try: 
         for i in tokenized:
             #words = nltk.word_tokenize(i)
             #tagged = nltk.pos_tag(words)
             #print i 
             print"***************"
             sentence = "u'%s'" % i
             print sentence
             print"***************"
             
             nlp = spacy.load('en')
             doc = nlp(sentence)
            
             for ent in doc.ents:
                print(ent.label_, ent.text)
             
     except Exception as e:
             print(str(e))
 
process_context()

