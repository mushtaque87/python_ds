#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 17 21:19:15 2017

@author: philips
"""
import nltk
import random
from nltk.corpus import movie_reviews

import pickle

from nltk.classify.scikitlearn import SklearnClassifier

from sklearn.naive_bayes import MultinomialNB, GaussianNB, BernoulliNB
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC, LinearSVC, NuSVC

documents = [(list(movie_reviews.words(fileid)),category) 
             for category in movie_reviews.categories()
             for fileid in movie_reviews.fileids(category)]

random.shuffle(documents)
#print documents[1]

all_words = []
for w in movie_reviews.words():
    all_words.append(w.lower())
   
print "\n"     
all_words = nltk.FreqDist(all_words)
print all_words.most_common(15)

print "\n" 
print (all_words["money"])

word_features = list(all_words.keys())[:3000]

def find_features(document):
    words = set(document)
    features = {}
    for w in word_features:
        features[w] = (w in words)
        #print features
        return features
    
#print((find_features(movie_reviews.words('neg/cv000_29416.txt'))))
featuresets = [(find_features(rev), category) for (rev, category) in documents]

#create train and test dataset 
training_set = featuresets[:1999]
testing_set = featuresets[1999:]


#Write a Classifier
classifier = nltk.NaiveBayesClassifier.train(training_set)
print ("Naive Bayes Algo acuracy percent :", (nltk.classify.accuracy(classifier,testing_set))*100)
classifier.show_most_informative_features()  

#==============================================================================
# How it will be reopend
#classifier_f = open("naivebaiyes.pickle","rb")
# classifier.pickle.load(classifier_f)
# classifier_f.close()
#==============================================================================

#Pickeling   
#==============================================================================
#How it will save
# save_classifier = open("naivebaiyes.pickle","wb")
# pickle.dump(classifier,save_classifier)
# save_classifier.close()
#==============================================================================

MNB_classifier = SklearnClassifier(MultinomialNB())
MNB_classifier.train(training_set)
print ("MNB_classifier Algo acuracy percent :", (nltk.classify.accuracy(MNB_classifier,testing_set))*100)
 
#==============================================================================
# Gaussian_classifier = SklearnClassifier(GaussianNB())
# Gaussian_classifier.train(training_set)
# print ("Gaussian_classifier Algo acuracy percent :", (nltk.classify.accuracy(Gaussian_classifier,testing_set))*100)
#  
#==============================================================================

Bernoulli_classifier = SklearnClassifier(BernoulliNB())
Bernoulli_classifier.train(training_set)
print ("Bernoulli_classifier Algo acuracy percent :", (nltk.classify.accuracy(Bernoulli_classifier,testing_set))*100)
 

Logistic_classifier = SklearnClassifier(LinearSVC())
Logistic_classifier.train(training_set)
print ("Logistic_classifier Algo acuracy percent :", (nltk.classify.accuracy(Logistic_classifier,testing_set))*100)
 

SGDC_classifier = SklearnClassifier(SGDClassifier())
SGDC_classifier.train(training_set)
print ("SGDC_classifier Algo acuracy percent :", (nltk.classify.accuracy(SGDC_classifier,testing_set))*100)
 

SVC_classifier = SklearnClassifier(SVC())
SVC_classifier.train(training_set)
print ("SVC_classifier Algo acuracy percent :", (nltk.classify.accuracy(SVC_classifier,testing_set))*100)
 

Linear_classifier = SklearnClassifier(LinearSVC())
Linear_classifier.train(training_set)
print ("Linear_classifier Algo acuracy percent :", (nltk.classify.accuracy(Linear_classifier,testing_set))*100)
 

NuSVC_classifier = SklearnClassifier(NuSVC())
NuSVC_classifier.train(training_set)
print ("NuSVC_classifier Algo acuracy percent :", (nltk.classify.accuracy(NuSVC_classifier,testing_set))*100)
 

