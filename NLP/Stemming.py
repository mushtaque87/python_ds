#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 12 22:38:49 2017

@author: philips
"""
#==============================================================================
# 
# Stemming gives you the root word for a series of word . 
# 
# Phthoning , pythonly , pythones : python
#==============================================================================


from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize

ps = PorterStemmer()

example_words_python = ['pythoning','pythonly', 'pythoning']

for words in example_words_python:
    print (ps.stem(words))
print ""

example_words_run = ['killer','killed','killing','will kill']
print ""

for words in example_words_run:
    print (ps.stem(words))
    
print ""
example_statement = 'I am killing few people just like the way i killed yesterday. I want to be a killer.'

words_kill = word_tokenize(example_statement)

for words in words_kill:
    print (ps.stem(words))