#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 13 08:28:47 2017

@author: philips
"""
#==============================================================================
# 
# Part of Speech tagging does exactly what it sounds like, it tags each word in a
#  sentence with the part of speech for that word. This means it labels words as noun, 
# adjective, verb, etc. PoS tagging also covers tenses of the parts of speech. 
#==============================================================================
#==============================================================================
# 
# 1.     CC	Coordinating conjunction
# 2.	    CD	Cardinal number
# 3.    	DT	Determiner
# 4.	     EX	Existential there
# 5.	     FW	Foreign word
# 6.	     IN	Preposition or subordinating conjunction
# 7.    	JJ	Adjective
# 8.	     JJR	Adjective, comparative
# 9.	     JJS	Adjective, superlative
# 10.	LS	List item marker
# 11.	MD	Modal
# 12.	NN	Noun, singular or mass
# 13.	NNS	Noun, plural
# 14.	NNP	Proper noun, singular
# 15.	NNPS	Proper noun, plural
# 16.	PDT	Predeterminer
# 17.	POS	Possessive ending
# 18.	PRP	Personal pronoun
# 19.	PRP$	Possessive pronoun
# 20.	RB	Adverb
# 21.	RBR	Adverb, comparative
# 22.	RBS	Adverb, superlative
# 23.	RP	Particle
# 24.	SYM	Symbol
# 25.	TO	to
# 26.	UH	Interjection
# 27.	VB	Verb, base form
# 28.	VBD	Verb, past tense
# 29.	VBG	Verb, gerund or present participle
# 30.	VBN	Verb, past participle
# 31.	VBP	Verb, non-3rd person singular present
# 32.	VBZ	Verb, 3rd person singular present
# 33.	WDT	Wh-determiner
# 34.	WP	Wh-pronoun
# 35.	WP$	Possessive wh-pronoun
# 36.	WRB	Wh-adverb
# 
#==============================================================================
#==============================================================================
# 
# Chunking in Natural Language Processing (NLP) is the process by which we group various words together by their part of speech tags. 
# 
# One of the most popular uses of this is to group things by what are called "noun phrases."
#  We do this to find the main subjects and descriptive words around them, 
# but chunking can be used for any combination of parts of speech.
# 
#==============================================================================


#==============================================================================
# Chinking is a part of the chunking process with natural language processing with NLTK. 
# A chink is what we wish to remove from the chunk. We define a chink in a
# very similar fashion compared to how we defined the chunk. 
# 
# The reason why you may want to use a chink is when your chunker is getting 
# almost everything you want, but is also picking up some things you don't want.
#  You could keep adding chunker rules, but it may be far easier to just specify a chink to 
#  remove from the chunk.
#==============================================================================

import nltk
from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer


train_text = state_union.raw("2005-GWBush.txt")
sample_text = state_union.raw("2006-GWBush.txt")

#train_text = "Hello !! Everyone how are you all . I am Mushtaque Ahmed"
#sample_text = "and i am going to be your machine learning trainer"


custom_sent_tokenizer = PunktSentenceTokenizer(train_text)
print custom_sent_tokenizer
tokenized = custom_sent_tokenizer.tokenize(sample_text)
print tokenized
def process_context():
    try: 
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            print tagged
            
            #chunkGram = r"""Chunk: {<RB.?>*<VB.?>*<NNP>+<NN>?}"""
            chunkGram = r"""Chunk: {<.*>+}
            }<VB.?|IN|OUT|TO>+{"""
            chunkParser = nltk.RegexpParser(chunkGram)
            chunked = chunkParser.parse(tagged)
            chunked.draw()
        
    except Exception as e:
            print(str(e))

process_context()