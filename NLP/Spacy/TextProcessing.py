#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 13:04:12 2017

@author: philips
"""

import spacy                           # See "Installing spaCy"
nlp = spacy.load('en')  
               # You are here.
#doc = nlp(u'Hello, spacy!')            # See "Using the pipeline"
#print([(w.text, w.pos_) for w in doc]) # See "Doc, Span and Token"

#==============================================================================
# doc = nlp.make_doc("Hello, spacy!")
# for proc in nlp.pipeline:
#     proc(doc)
#==============================================================================


def count_entity_sentiment(nlp, texts):
    '''Compute the net document sentiment for each entity in the texts.'''
    entity_sentiments = collections.Counter(float)
    for doc in nlp.pipe(texts, batch_size=1000, n_threads=4):
        for ent in doc.ents:
            entity_sentiments[ent.text] += doc.sentiment
    return entity_sentiments

def load_nlp(lstm_path, lang_id='en'):
    def create_pipeline(nlp):
        return [nlp.tagger, nlp.entity, SentimentAnalyser.load(lstm_path, nlp)]
    return spacy.load(lang_id, create_pipeline=create_pipeline)