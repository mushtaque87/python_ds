#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 10 09:14:56 2017

@author: philips
"""

from nltk.tokenize import sent_tokenize,word_tokenize

example_test = "Hello Mr. Smith, how are you doing today? The weather is great "

print(sent_tokenize(example_test))  