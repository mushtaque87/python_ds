#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  9 12:10:15 2017

@author: philips
"""

'''Use bag of word tehchnqie to transform documents into vectors'''

from sklearn.feature_extraction.text import CountVectorizer

#instantiate the vectoriser
vectorizer = CountVectorizer()

#create documents 
document_1 = 'Hi how are you '
document_2 = 'today is a very very very pleasent day !!! We can have fun fun fun'
document_3 = 'Lets have fun'

listofdocment =[document_1,document_2,document_3]

#fit them in a bag of words
bag_of_words = vectorizer.fit(listofdocment)

bag_of_words = vectorizer.transform(listofdocment)
print bag_of_words
type(bag_of_words)

print vectorizer.vocabulary_.get('very')
print vectorizer.vocabulary_.get('fun')
