#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 15 11:54:55 2017

@author: philips
"""
#==============================================================================
# Named entity recognition is useful to quickly find out what the subjects 
# of discussion are. NLTK comes packed full of options for us. 
# We can find just about any named entity, or we can look for specific ones.
# 
# NLTK can either recognize a general named entity, or it can even recognize locations,
#  names, monetary amounts, dates, and more. 
#==============================================================================



import nltk
from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer

import spacy
nlp = spacy.load('en')
doc = nlp(u'London is a big city in the United Kingdom.')
for ent in doc.ents:
    print(ent.label_, ent.text)
    
    
train_text = state_union.raw("2005-GWBush.txt")
sample_text = state_union.raw("2006-GWBush.txt")

#train_text = "Hello !! Everyone how are you all . I am Mushtaque Ahmed"
#sample_text = "and i am going to be your machine learning trainer"


custom_sent_tokenizer = PunktSentenceTokenizer(train_text)
print custom_sent_tokenizer
tokenized = custom_sent_tokenizer.tokenize(sample_text)
print tokenized
def process_context():
    try: 
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            
            nameEnt = nltk.ne_chunk(tagged)
            nameEnt.draw()
            
    except Exception as e:
            print(str(e))

process_context()