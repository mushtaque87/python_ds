#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 13:04:11 2017

@author: philips
"""

#https://textblob.readthedocs.io/en/dev/quickstart.html#noun-phrase-extraction

from textblob import TextBlob
wiki = TextBlob("Python is a high-level, general-purpose programming language.")
print(wiki.tags)
print(wiki.noun_phrases)



testimonial = TextBlob("Textblob is amazingly simple to use. What great fun!")
print(testimonial.sentiment)


testimonial = TextBlob("Textblob is too difficult to use. What bad code!")
print(testimonial.sentiment)
print""

zen = TextBlob("Beautiful is better than ugly. "
                "Explicit is better than implicit. "
                "Simple is better than complex.")
print zen.words
print zen.sentences
print""

sentence = TextBlob('Use 4 spaces per indentation level.')
sentence.words
print(sentence.words[2].singularize())
print(sentence.words[5].pluralize())
print""

chinese_blob = TextBlob(u"电视")
print chinese_blob.translate(from_lang="zh-CN", to='en')
print""

b = TextBlob(u"اللہ کے نام کے ساتھ جو بے انتہا رحم کرنے والا، بِن مانگے دینے والا (اور) بار بار رحم کرنے ")
print b.detect_language()
print b.translate(  to='en')


print "Hello TextBlob"


