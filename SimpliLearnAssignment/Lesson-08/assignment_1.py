'''
Created on 28-Jun-2017

@author: philips
'''
import pandas as pd
import numpy as np

if __name__ == '__main__':
    pass
#names = np.loadtxt("/Users/philips/Documents/python_ds/SimpliLearnAssignment/pima_indians_diabetes.names")
#print name
dataset = pd.read_csv("/Users/philips/Documents/python_ds/SimpliLearnAssignment/pima_indians_diabetes.data")
dataframe = pd.DataFrame(dataset)
dataframe.columns = ['no_of_pregnancy','plasma_glucose','diastolic_bp','tricepskin_fold','serum_insulin','bmi','pedigree','age','diabetes_status']
print dataframe

