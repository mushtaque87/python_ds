'''
Created on 05-Jun-2017

@author: philips
'''

import pandas as pd
'''
 Here's the algorithm, predict the passenger survived if:
    1) If the passenger is female or
    2) if his/her socioeconomic status is high AND if the passenger is under 18
    
    You can access the socioeconomic status of a passenger via passenger['Pclass']:
    High socioeconomic status -- passenger['Pclass'] is 1
    Medium socioeconomic status -- passenger['Pclass'] is 2
    Low socioeconomic status -- passenger['Pclass'] is 3
    
'''

passenger_df = pd.read_csv('/Users/philips/Documents/python_ds/dataset/train.csv',low_memory=False)
print(passenger_df[['Sex','Pclass','Age']])

predictions = {}
survival_count = 0  #number of people survived count.
for count, passenger in passenger_df.iterrows():
    passenger_id = passenger['PassengerId']
    
    if passenger['Sex'] == 'female' :
        
        predictions[passenger_id] =  1
        survival_count += 1
    elif (passenger['Pclass'] == 1)  & (passenger['Age'] < 18) :
                predictions[passenger_id] =  1
                survival_count += 1
    
    else:
                predictions[passenger_id] =  0

    
print(predictions)
print(survival_count)

survived_count = 0
for count, passenger in passenger_df.iterrows():
    
    if passenger['Survived'] == 1 :
        survived_count += 1 
print("")
print(survived_count)

