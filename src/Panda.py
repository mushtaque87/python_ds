'''
Created on 30-May-2017

@author: philips
'''

import pandas as pd
from pandas.core.frame import DataFrame



 
'''creating disctionary'''
data = {'name':pd.Series(['alex','john','Allen','rose']),
        'age':pd.Series([22,38,26,35]),
        'fare' :pd.Series([7.25,71.83,8.05]),
        'survived'  :pd.Series([False,True,True,False])
        }
df = DataFrame(data)
 
print(df) 
print("\n\n")
   
#series = pd.Series(['Dave', 'Cheng-Han', 'Udacity', 42, -1789710578])
#print (series)
football_data = {'year': [2010, 2011, 2012, 2011, 2012, 2010, 2011, 2012],
            'team': ['Bears', 'Bears', 'Bears', 'Packers', 'Packers', 'Lions',
                     'Lions', 'Lions'],
            'wins': [11, 8, 10, 15, 11, 6, 10, 4],
            'losses': [5, 8, 6, 1, 5, 10, 6, 12]}
football = pd.DataFrame(football_data)
print (football)
print("") 
     
print(football.head(2))
print("")
print(football.tail(3))
print("")
print(football.dtypes)
print("")
print(football.describe(include='all'))  #Mathematical operation on all the numberical coloumns . include='all' does for all columns
 

