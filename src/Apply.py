'''
Created on 03-Jun-2017

@author: philips
'''

import pandas as pd
import numpy as np
from pandas.core.frame import DataFrame

''' Sichin Medal Data - Udacity'''
countries = ['Russian Fed.', 'Norway', 'Canada', 'United States',
                 'Netherlands', 'Germany', 'Switzerland', 'Belarus',
                 'Austria', 'France', 'Poland', 'China', 'Korea', 
                 'Sweden', 'Czech Republic', 'Slovenia', 'Japan',
                 'Finland', 'Great Britain', 'Ukraine', 'Slovakia',
                 'Italy', 'Latvia', 'Australia', 'Croatia', 'Kazakhstan']

gold = [13, 11, 10, 9, 8, 8, 6, 5, 4, 4, 4, 3, 3, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0]
silver = [11, 5, 10, 7, 7, 6, 3, 0, 8, 4, 1, 4, 3, 7, 4, 2, 4, 3, 1, 0, 0, 2, 2, 2, 1, 0]
bronze = [9, 10, 5, 12, 9, 5, 2, 1, 5, 7, 1, 2, 2, 6, 2, 4, 3, 1, 2, 1, 0, 6, 2, 1, 0, 1]

# your code here
olympic_medal_counts_array = {'country_name':countries,
                                    'gold' : gold,
                                    'silver':silver,
                                    'bronze':bronze}
olympic_medal_counts_df = pd.DataFrame(olympic_medal_counts_array) 

''' Using the dataframe's apply method, create a new Series called 
    avg_medal_count that indicates the average number of gold, silver,
    and bronze medals earned amongst countries who earned at 
    least one medal of any kind at the 2014 Sochi olympics.  Note that
    the countries list already only includes countries that have earned
    at least one medal.'''

countries_with_atleast_onebronze = olympic_medal_counts_df[(olympic_medal_counts_df['bronze'] >= 1)]
print(countries_with_atleast_onebronze)
print("")
print(countries_with_atleast_onebronze[['gold','silver','bronze']].apply(np.mean))

'''
    Imagine a point system in which each country is awarded 4 points for each
    gold medal,  2 points for each silver medal, and one point for each 
    bronze medal.  

    Using the numpy.dot function, create a new dataframe called 
    'olympic_points_df' that includes:
        a) a column called 'country_name' with the country name
        b) a column called 'points' with the total number of points the country
           earned at the Sochi olympics.
'''
#Udacity Approch
medal_count = olympic_medal_counts_df[['gold','silver','bronze']]
points_array = np.dot(medal_count,[4,2,1])
olympic_points_df = DataFrame({'country_name':olympic_medal_counts_df['country_name'],'points': pd.Series(points_array)})
print(olympic_points_df)


''' My approch

points_array = [4,2,1]     
olympic_points_array = []
for count, row in olympic_medal_counts_df.iterrows():
    medal_array = [olympic_medal_counts_df['gold'].loc[count],
                   olympic_medal_counts_df['silver'].loc[count],
                   olympic_medal_counts_df['bronze'].loc[count]]
    points = np.dot(points_array,medal_array)   
    olympic_points_array.append([olympic_medal_counts_df['country_name'].loc[count],points])
    #===========================================================================
    # if len(olympic_points_array) == 0: 
    #     olympic_points_array = [olympic_medal_counts_df['country_name'].loc[count],points]  
    # else:
    #     olympic_points_array.append([olympic_medal_counts_df['country_name'].loc[count],points] )
    #===========================================================================
      
olympic_points_df = pd.DataFrame[olympic_points_array]    
print(olympic_points_df)
'''       
           