'''
Created on 04-Jun-2017

@author: philips
'''
import pandas as pd
from decimal import DecimalException

'''Here's a simple heuristic to start off:
       1) If the passenger is female, your heuristic should assume that the
       passenger survived.
       2) If the passenger is male, you heuristic should
       assume that the passenger did not survive.
'''
#===============================================================================
# http://www.kaggle.com/c/titanic-gettingStarted/data.
# https://s3.amazonaws.com/content.udacity-data.com/courses/ud359/titanic_data.csv     
#      
#===============================================================================


passenger_df = pd.read_csv('/Users/philips/Documents/python_ds/dataset/train.csv',low_memory=False)
#passenger_df = pd.DataFrame(passenger_data)
print(passenger_df)
survival_array = []
predictions = {}
for count, passenger in passenger_df.iterrows():
    #print("Passenger Gender :{}".format(passenger_df['Sex'].iloc[count]))
    #print("")
    #print(passenger)
    passenger_id = passenger['PassengerId']

    if passenger_df['Sex'].loc[count] == 'female':
        predictions[passenger_id] = 1
        survival_array.extend('1')
    else:
        predictions[passenger_id] = 0
        survival_array.extend('0')
        
 
prediction = {'PassengerId':pd.Series(passenger_df['PassengerId']),'survived':pd.Series(survival_array)}       
print("")
print(pd.DataFrame(prediction))

'''Udacity Approch'''

passenger_df = pd.read_csv('/Users/philips/Documents/python_ds/dataset/train.csv',low_memory=False)
#passenger_df = pd.DataFrame(passenger_data)
print(passenger_df)
predictions = {}
for count, passenger in passenger_df.iterrows():
    #print("Passenger Gender :{}".format(passenger_df['Sex'].iloc[count]))
    #print("")
    #print(passenger)
    passenger_id = passenger['PassengerId']

    if passenger_df['Sex'].loc[count] == 'female':
        predictions[passenger_id] = 1
    else:
        predictions[passenger_id] = 0
        
 
print("")
print(predictions)