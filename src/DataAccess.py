'''
Created on 01-Jun-2017

@author: philips
'''
import pandas as pd
import numpy as np
from django.contrib.gis.shortcuts import numpy

''' Sichin Medal Data - Udacity'''
countries = ['Russian Fed.', 'Norway', 'Canada', 'United States',
                 'Netherlands', 'Germany', 'Switzerland', 'Belarus',
                 'Austria', 'France', 'Poland', 'China', 'Korea', 
                 'Sweden', 'Czech Republic', 'Slovenia', 'Japan',
                 'Finland', 'Great Britain', 'Ukraine', 'Slovakia',
                 'Italy', 'Latvia', 'Australia', 'Croatia', 'Kazakhstan']

gold = [13, 11, 10, 9, 8, 8, 6, 5, 4, 4, 4, 3, 3, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0]
silver = [11, 5, 10, 7, 7, 6, 3, 0, 8, 4, 1, 4, 3, 7, 4, 2, 4, 3, 1, 0, 0, 2, 2, 2, 1, 0]
bronze = [9, 10, 5, 12, 9, 5, 2, 1, 5, 7, 1, 2, 2, 6, 2, 4, 3, 1, 2, 1, 0, 6, 2, 1, 0, 1]

# your code here
olympic_medal_counts_array = {'country_name':countries,
                                    'gold' : gold,
                                    'silver':silver,
                                    'bronze':bronze}
olympic_medal_counts_df = pd.DataFrame(olympic_medal_counts_array) 
print(olympic_medal_counts_df)
print("")
print(olympic_medal_counts_df['country_name'])  #Access a column
print("")
print(olympic_medal_counts_df[['country_name','gold']]) #Access multiple column
print("")
print(olympic_medal_counts_df.loc[22]) #locate an element by index
print("")
print(olympic_medal_counts_df.iloc[[22]])
print("")
'''conditional location/searching'''
print(olympic_medal_counts_df[olympic_medal_counts_df['gold']>10]) #Return the rows where value of "gold" > 10
print("")
print(olympic_medal_counts_df['country_name'][olympic_medal_counts_df['gold'] >= 10]) #Return the column where value of "gold" > 10

'''
Also a couple pointers:
1) Selecting a single column from the DataFrame will return a Series
2) Selecting multiple columns from the DataFrame will return a DataFrame
'''

''' Slicing and Boolean indexing'''
print(olympic_medal_counts_df[3:5])
print("")
print(olympic_medal_counts_df[(olympic_medal_counts_df.gold > 10) & (olympic_medal_counts_df.silver > 2)])
print("")
'''Compute the average number of bronze medals earned by countries who earned at least one gold medal.'''
countries_earned_gold = olympic_medal_counts_df[(olympic_medal_counts_df.gold >=1 )]
print(countries_earned_gold)
print(np.mean(countries_earned_gold['bronze']))

#=================== Another way - udacity
# countries_earned_gold = olympic_medal_counts_df['bronze'][(olympic_medal_counts_df.gold >=1)]
# print(np.mean(countries_earned_gold))
#===============================================================================



#print(olympic_medal_counts_df[(olympic_medal_counts_df.apply(np.mean)) & (olympic_medal_counts_df.silver > 2)])

#===============================================================================
# '''WAP to find the total number of medals won by each nation'''
# for count, row in olympic_medal_counts_df.iterrows():  
#     total_medal = olympic_medal_counts_df.gold + olympic_medal_counts_df.silver + olympic_medal_counts_df.bronze
#     print(total_medal)
#     print(olympic_medal_counts_df['country_name'].loc[count])
#    
#     print("")
#===============================================================================


