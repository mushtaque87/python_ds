'''
Created on 12-Jun-2017

@author: philips
'''
import csv
if __name__ == '__main__':
    pass

'''
    Filenames is a list of MTA Subway turnstile text files. A link to an example
    MTA Subway turnstile text file can be seen at the URL below:
    http://web.mta.info/developers/data/nyct/turnstile/turnstile_110507.txt
    
    As you can see, there are numerous data points included in each row of the
    a MTA Subway turnstile text file. 

    You want to write a function that will update each row in the text
    file so there is only one entry per row. A few examples below:
    A002,R051,02-00-00,05-28-11,00:00:00,REGULAR,003178521,001100739
    A002,R051,02-00-00,05-28-11,04:00:00,REGULAR,003178541,001100746
    A002,R051,02-00-00,05-28-11,08:00:00,REGULAR,003178559,001100775
    
    Write the updates to a different text file in the format of "updated_" + filename.
    For example:
        1) if you read in a text file called "turnstile_110521.txt"
        2) you should write the updated data to "updated_turnstile_110521.txt"

    The order of the fields should be preserved. Remember to read through the 
    Instructor Notes below for more details on the task. 
    
    In addition, here is a CSV reader/writer introductory tutorial:
    http://goo.gl/HBbvyy
    '''
def copyfile():
    for line in reader_in:
        new_line = [line[0],line[1],line[2],line[3],line[4],line[5],line[6],line[7]]
        print new_line
        print ""
        writer_out.writerow(new_line)
    return
    


def appendFile():
    fileList = ['/Users/philips/Documents/python_ds/dataset/file_1.txt','/Users/philips/Documents/python_ds/dataset/file_2.txt']
    print fileList
    file_out = open('/Users/philips/Documents/python_ds/dataset/new_file.txt','w')
    writer_out = csv.writer(file_out,delimiter =',')
    writer_out.writerow(['filename','lineno','version','date','time'])
    for file in fileList:
        file_in =  open(file,'r')
        reader_in = csv.reader(file_in,delimiter = ',')
        for line in reader_in:
            print line
            print ""
            writer_out.writerow(line)
    return
        
     
                  


file_in = open('/Users/philips/Documents/python_ds/dataset/turnstile_110507.txt','r')
file_out = open('/Users/philips/Documents/python_ds/dataset/update_turnstile_110507.txt','w')

reader_in = csv.reader(file_in,delimiter = ',')
writer_out = csv.writer(file_out,delimiter =',')
#copyfile()
appendFile()
    