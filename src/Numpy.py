'''
Created on 30-May-2017
 
@author: philips
'''



import numpy as np 
 
import scipy as sp
import matplotlib
from numpy import char, string0


print("######### Numpy ########")

'''Array'''
 
number = [1,2,3,4,5]
mean = np.mean(number)
print(mean)   

median = np.median(number)
print(median) 

std = np.std(number)
print(std)

'''1D array'''

one_DArray = np.array([1,2,3,4])
print("1D array is %s" %one_DArray)


''' Indexing and Slicing an array'''

print(one_DArray[0]) #Access array from 0 -> an index  '''
print(one_DArray[:3]) #Access array from 0 -> an index'''


one_DArray[0] = 5  #Assign a value to an index of an array 
print(one_DArray)

'''2D array'''

two_D_array = np.array([[1,2,3,4],["a","b","c","d"]])
print("2D array is \n %s" %two_D_array)

print(two_D_array[1][2]) # Print an item [ros,coloumn]
print(two_D_array[1,1]) 

print(two_D_array[:,2])  # Print Coloumn
print(two_D_array[1,:]) # Print Row

'''3D array'''
#three_D_array = np.array([[1,2,4,6],[3,9,4,16],["x","y","z"]])  # is a scalar array
#print((three_D_array[1]))

three_D_array = np.array([[1,2,4,6],[3,9,4,16],["x","y","z","o"]])
print(three_D_array)


''' If we are inserting array inside an np.array , then all the arrays should have equal items to be a 
n Dimensional array(square matrix)
or else it will be consider an 1D array'''


''' Arithmetic Operations'''
list_a = np.array([1,2,3])
list_b = np.array([2,3,4])
list_c = np.array([5,3,2])
print(list_a*list_b*list_c)
print(list_a+list_b-list_c)
print(list_a+list_b*list_c) #First multiplication then addition . BODMAS
print("\n")
print(one_DArray)
print(np.mean(one_DArray))
print(np.median(one_DArray))
print(np.std(one_DArray))
print(np.dot(one_DArray,one_DArray))
    