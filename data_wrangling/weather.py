'''
Created on 10-Jun-2017

@author: philips
'''
import pandas as pd
import numpy as np
from pandasql import sqldf
from ctypes import cast
from numpy import integer
from datetime import datetime, date

if __name__ == '__main__':
    pass

'''    https://s3.amazonaws.com/content.udacity-data.com/courses/ud359/weather_underground.csv '''



def meanMinTemp(df):
    df_new = df[(df['mintempi'] > 55) & (df['rain'] == 1) ]
    print df_new
    averageMinTemp = np.mean(df_new['mintempi'])
    print averageMinTemp
    return averageMinTemp

def num_rainy_days(weather_data):
    '''
    This function should run a SQL query on a dataframe of
    weather data.  The SQL query should return one column and
    one row - a count of the number of days in the dataframe where
    the rain column is equal to 1 (i.e., the number of days it
    rained).  The dataframe will be titled 'weather_data'. You'll
    need to provide the SQL query.  You might find SQL's count function
    useful for this exercise.  You can read more about it here:
    
    https://dev.mysql.com/doc/refman/5.1/en/counting-rows.html
    '''
    q = """ Select Count(*) from weather_data where rain == 1"""
    
    #Execute your SQL command against the pandas frame
    rainy_days = sqldf(q.lower(), locals())
    print rainy_days
    return rainy_days

def max_temp_aggregate_by_fog(weather_data):
    '''
    This function should run a SQL query on a dataframe of
    weather data.  The SQL query should return two columns and
    two rows - whether it was foggy or not (0 or 1) and the max
    maxtempi for that fog value (i.e., the maximum max temperature
    for both foggy and non-foggy days).  The dataframe will be 
    titled 'weather_data'. You'll need to provide the SQL query.'''

    q = """SELECT fog,maxtempi from  weather_data Group by fog"""
    
    #Execute your SQL command against the pandas frame
    foggy_days = sqldf(q.lower(), locals())
    print foggy_days
    return foggy_days
    
def avg_weekend_temperature(weather_data):
    '''
    This function should run a SQL query on a dataframe of
    weather data.  The SQL query should return one column and
    one row - the average meantempi on days that are a Saturday
    or Sunday (i.e., the the average mean temperature on weekends).
    The dataframe will be titled 'weather_data' and you can access
    the date in the dataframe via the 'date' column.
    
    You'll need to provide  the SQL query.
    
    Also, you can convert dates to days of the week via the 'strftime' keyword in SQL.
    For example, cast (strftime('%w', date) as integer) will return 0 if the date
    is a Sunday or 6 if the date is a Saturday.
    '''
   # weather_data.apply(cast(strftime('%w', date) as integer))

    weather_data['day'] = weather_data['date'].apply(lambda x:pd.to_datetime(str(x),  format='%d/%m/%y'))
    print weather_data['day'] 
    
    q = """SELECT date from weather_data WHERE ;"""
    #q = """SELECT strftime('%Y %m %d','now'); """
    
   
    
    #Execute your SQL command against the pandas frame
    mean_temp_weekends = sqldf(q.lower(), locals())
    print mean_temp_weekends    
    return mean_temp_weekends

def avg_min_temperature(weather_data):
    '''
    This function should run a SQL query on a dataframe of
    weather data. More specifically you want to find the average
    minimum temperature (mintempi column of the weather dataframe) on 
    rainy days where the minimum temperature is greater than 55 degrees.

    ''' 
    q = """Select mintempi from weather_data where rain = 1 AND mintempi > 55"""
   
    #Execute your SQL command against the pandas frame
    avg_min_temp_rainy = sqldf(q.lower(), locals())
    print avg_min_temp_rainy
    
    avg_min_temp_rainy = np.mean(avg_min_temp_rainy)
    print avg_min_temp_rainy
    
    return avg_min_temp_rainy

  
path_to_csv = '/Users/philips/Documents/python_ds/dataset/weather_underground.csv'
df = pd.read_csv(path_to_csv)

#meanMinTemp(df)
#num_rainy_days(df)
#max_temp_aggregate_by_fog(df)
avg_weekend_temperature(df)
#avg_min_temperature(df)