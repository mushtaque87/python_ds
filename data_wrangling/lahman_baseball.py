'''
Created on 05-Jun-2017

@author: philips
'''
import pandas as pd
import numpy as np

'''
1) Write a function that reads a csv
   located at "path_to_csv" into a pandas dataframe and adds a new column
   called 'nameFull' with a player's full name.
   
   For example:
      for Hank Aaron, nameFull would be 'Hank Aaron', 
   
   2) Write the data in the pandas dataFrame to a new csv file located at
   path_to_new_csv
   
       # The dataset can be downloaded from this website: http://www.seanlahman.com/baseball-archive/statistics

'''

def add_full_name(path_to_csv, path_to_new_csv):
    player_df = pd.read_csv(path_to_csv,low_memory = False)
    #print (passenger_df)
    
   #============================================================================
   #  # My Approch
   #  player_df['fullname'] = player_df[['nameFirst','nameLast']].apply(lambda x: "{} {}".format(x[0],x[1]),axis = 1)
   # 
   #============================================================================
   
    #Udacity Approch
    player_df['fullname'] = player_df['nameFirst'] + " " +player_df['nameLast'] 
    print(player_df['fullname'])
    
    player_df['fullname'].to_csv(path_to_new_csv)
    return;


path_to_csv = '/Users/philips/Documents/python_ds/dataset/Master.csv'
path_to_new_csv = '/Users/philips/Documents/python_ds/dataset/MasterNew.csv'
add_full_name(path_to_csv, path_to_new_csv)
    
    

    