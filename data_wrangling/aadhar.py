'''
Created on 06-Jun-2017

@author: philips
'''
import pandas as pd
from pandasql import sqldf
'''
Fetch registrar,enrolment_agency data for first 50 rows
'''
def select_first_50(aadhaar_data):
    query = "select registrar,enrolment_agency from aadhaar_data LIMIT 10;"
    aadhar_data = sqldf(query.lower(), locals())
    return aadhar_data

'''
Print all the pincode of State Gujrat 
'''
    # https://s3.amazonaws.com/content.udacity-data.com/courses/ud359/aadhaar_data.csv


def select_pincode(aadhaar_data):
    query = 'SELECT pin_code,state from aadhaar_data where state = \'Gujarat\' ' 
    aadhar_data = sqldf(query.lower(), locals())
    return aadhar_data


aadhaar_data = pd.read_csv('/Users/philips/Downloads/aadhaar_data.csv')
aadhaar_data.rename(columns = lambda x: x.replace(' ', '_').lower(), inplace=True) #rename column
#aadhar_queried_data = select_first_50(aadhaar_data)
aadhar_queried_data = select_pincode(aadhaar_data)

print(aadhar_queried_data)



