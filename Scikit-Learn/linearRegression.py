'''
Created on 21-Jun-2017

@author: philips
'''
import numpy as np
import pandas as pd
from sklearn.datasets  import load_boston
from scipy.stats.mstats_basic import linregress
from matplotlib.pyplot import plot



if __name__ == '__main__':
    pass
boston_dataset = load_boston()
#print boston_dataset
#Create a panda dataframe and store the data
df_boston = pd.DataFrame(boston_dataset.data)

#Append price,target, as a new column to the dataset
df_boston.coloumns = boston_dataset.feature_names
df_boston['Price'] = boston_dataset.target
#print df_boston.head(5)

#Assign features on X-axis
X_features = boston_dataset.data

#Assign feature on Y-axis
Y_target = boston_dataset.target

#import linear model - the estimator
from sklearn.linear_model import LinearRegression
lineReg = LinearRegression()

#fit data into the estimator
lineReg.fit(X_features,Y_target)

#print the intercept 
print "the estimated intercept %.2f" %lineReg.intercept_

#print the co-efficient 
print "the coefficient is %d" %len(lineReg.coef_)

#train model split the whole dataset into train and test dataset
from sklearn import  model_selection
X_train , X_test , Y_train , Y_test = model_selection.train_test_split(X_features,Y_target)

#print the share of training and testing data 
print X_train.shape,X_test.shape,Y_train.shape , Y_test.shape

#fit the training set into the model
lineReg.fit(X_train,Y_train)

print pd.DataFrame(X_test)
print "-------------"

print pd.DataFrame(Y_test)
print "-------------"


#the mean square error (MSE) or residual sum of error 
print "msg value is %.2f" % np.mean((lineReg.predict(X_test)-Y_test) ** 2)

#calculate the variance
print "Variance is %.2f" % lineReg.score(X_test, Y_test)
