'''
Created on 22-Jun-2017

@author: philips
'''
import pandas as pd
import numpy as np
from sklearn.datasets import load_iris
from codeop import _features
from sklearn.datasets  import load_boston


if __name__ == '__main__':
    pass
#load dataset 
iris_dataset = load_iris()
df_iris = pd.DataFrame(iris_dataset.data)
print iris_dataset
#display the dataset type
type(iris_dataset)

#view information using dataset built in method 
print iris_dataset['DESCR']

#view features
print iris_dataset.feature_names

#view target
print iris_dataset.target

#assign feature data to X axis
X_feature = iris_dataset.data

#assign target to Y - axis
Y_target = iris_dataset.target

'''knn classifier'''

from sklearn.neighbors import KNeighborsClassifier

#instantiate the Knn estimator
knn = KNeighborsClassifier(n_neighbors=1)

#print the knn
print knn
#fit data into knn model
knn.fit(X_feature, Y_target)

#create object with new value for prediction 
X_new = [[3,5,4,1],[5,3,4,2]]

#predict the outcome of the new object by knn classifier
knn.predict(X_new)

'''Logistic Regression'''

#use the Logistic Regression model
from sklearn.linear_model import LogisticRegression
logreg = LogisticRegression()

#fit data into logistic regression estimator
logreg.fit(X_feature, Y_target)

#predict the outcome using Logistic Regression
logreg.predict(X_new)



